import Vue from 'vue';
import Router from 'vue-router';
import ShellGen from '../components/ShellGen.vue';
import Ping from '../components/Ping.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'ShellGen',
      component: ShellGen,
    },
    {
      path: '/ping',
      name: 'Ping',
      component: Ping,
    },
  ],
});
