import svgwrite
from svgwrite import cm, mm
import json
import random
import math
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.geometry import box
from shapely.ops import nearest_points
from shapely import affinity

def sqr_dist_between_tuples(p1,p2):
    return ((p1[0] - p2[0]) ** 2) + ((p1[1] - p2[1]) ** 2)

def shell(size, seed):
    """The shell generator.
    Using just a seed and size, we will generate a possible shell"""

    with open('sizedict.json','r') as string:
        sizedict = json.load(string)

    # Set seed
    random.seed(seed)

    # Now we determine our target tile count
    tile_count = random.randint(sizedict[size]['min'],sizedict[size]['max'])

    # Now, we work out our canvas size:
    bound_dim = math.ceil(math.sqrt(tile_count)) + 2
    
    # Create the svg drawing base
    #dwg = svgwrite.Drawing(filename='shell.svg', debug=True,size=(1000,1000))
    #hlines = dwg.add(dwg.g(id='hlines', stroke='green'))
    #for y in range(bound_dim):
    #    hlines.add(dwg.line(start=(2*cm, (2+y)*cm), end=(18*cm, (2+y)*cm)))
    #vlines = dwg.add(dwg.g(id='vline', stroke='blue'))
    #for x in range(bound_dim):
    #    vlines.add(dwg.line(start=((2+x)*cm, 2*cm), end=((2+x)*cm, 21*cm)))

    # Let's make our first polygon
    shell = box(0,0,bound_dim,bound_dim)

    # Now we start chipping away!
    while shell.area > tile_count:
        # Let's pick a new vertex within the current shape
        x, y = shell.exterior.coords.xy
        p1 = Point(random.randint(min(x),max(x)), 
        random.randint(min(y),max(y)))
        
        # If the new point 'p' isn't in the polygon, try again!
        # (TODO - work out a better way of doing this)
        while p1.within(shell) == False:
            p1 = Point(random.randint(min(x),max(x)), 
            random.randint(min(y),max(y)))
        
        # Now we have a point in the polygon
        # First, we get the closest point on the polygon
        p2, p2a = nearest_points(shell.boundary, p1)

        # We work out which point to remove from the polygon
        # Set the sqr min dist to the maximum possible
        sqr_min_dist = 2 * (bound_dim ** 2)

        # Now find the closest exterior coord to remove
        # TODO - is there a better way of doing this?
        for index, coord in enumerate(shell.exterior.coords):
            # Ignore the last coord as it is a duplicate
            if(index == len(shell.exterior.coords) - 1):
                break
            sqr_dist = sqr_dist_between_tuples(coord, p2.coords[0])
            if sqr_dist < sqr_min_dist:
                sqr_min_dist = sqr_dist
                index_to_remove = index
                # Check to see what vertex is missing
                if p1.x == p2.x:
                    p3 = (coord[0], p1.y)
                else:
                    p3 = (p1.x, coord[1])

        coords = []
        prev_coord = (x[len(x)-1],y[len(y)-1])
        for index, coord in enumerate(shell.exterior.coords):
            # Ignore the last coord as it is a duplicate
            if(index == len(shell.exterior.coords) - 1):
                break
            if index != index_to_remove:
                coords.append(coord)
            else:
                # We now work out what order to add the new vertices
                if sqr_dist_between_tuples(p2.coords[0], prev_coord) > \
                    sqr_dist_between_tuples(p1.coords[0], prev_coord):
                    coords.append(p3)
                    coords.append(p1.coords[0])
                    coords.append(p2.coords[0])
                else:
                    coords.append(p2.coords[0])
                    coords.append(p1.coords[0])
                    coords.append(p3)
            prev_coord = coord
        
        # Add the first coord again to tie up the polygon
        coords.append(coords[0])
        shell = Polygon(coords)

        # Boost the size!
        shell1 = affinity.scale(shell,xfact=10,yfact=10, origin=(0,0))

        dwg = svgwrite.Drawing(filename='shell.svg', debug=True,size=(1000,1000))
        
        shapes = dwg.add(dwg.g(id='shapes', stroke='red'))

        shapes.add(dwg.polygon(shell1.exterior.coords))

        dwg.save()

    return dwg.tostring()

if __name__ == '__main__':
    shell('small',1)