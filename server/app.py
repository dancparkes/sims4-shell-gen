from flask import Flask, jsonify, request, Response
from flask_cors import CORS
from shellgen import shell

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

# get the svg
@app.route('/shellgen', methods=['GET'])
def get_svg():
    response = Response(shell(),
                    mimetype="image/svg+xml")

    return response

if __name__ == '__main__':
    app.run()