# sims4-shell-generator

A Django app that takes some params and outputs a shell for Sims4 houses

## Inputs

### Challenge Sizes


- Pick a challenge:
    - Microhome: 4 & 32
    - Tiny: 33 - 64
    - Small: 65 - 100

We keep these apart from Microhome which will use a min of 10 because who builds a Sims house with just 4 tiles?!
- Min and max number of sides
- Maximum number of tiles
- Number of floors

### TODO
- Manage diagonals